from django.contrib import admin
from .models import Textbox, Tabledata
from tinymce.widgets import TinyMCE
from django.db import models
from django.db.models.functions import Lower
from django.utils.translation import ugettext_lazy
# Register your models here.



class TextboxAdmin(admin.ModelAdmin):


    formfield_overrides = {
        models.TextField: {'widget': TinyMCE()},
        }
    
    list_display = ('page', 'box_no', 'lang')
    search_fields = ['page']

    def get_ordering(self, request):
        return [Lower('page')]  # sort case insensitive


admin.site.register(Textbox,TextboxAdmin)
admin.site.register(Tabledata)
admin.site.site_header = "prostatecancer.me"
admin.site.site_title = "prostatecancer.me"