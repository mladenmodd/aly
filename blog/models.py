from django.db import models

# Create your models here.

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)

PAGES = (
    (0,"none"),
    (1,"index"),
    (2,"about"),
    (3,"prostate"),
    (4,"prostatecancer"),
    (5,"mena"),
    (6,"riskfactors"),
    (7,"screening"),
    (8,"dad"),
    (9,"treatment"),
    (10,"questions"),
    (11,"caring"),
    (12,"journey"),
    (13,"index"),
    (14,"well"),
    (15,"involved"),
    (16, "privacy"),
    (17, "disclamer"),
)

LANG = (
    (0,"eng"),
    (1,"ab"),
)

pgs = {
    '0' : "none",
    '1' : "index",
    '2' : "about",
    '3' : "prostate",
    '4' : "prostatecancer",
    '5' : "mena",
    '6' : "riskfactors",
    '7' : "screening",
    '8' : "dad",
    '9' : "treatment",
    '10' : "questions",
    '11' : "caring",
    '12' : "journey",
    '13' : "index",
    '14' : "well",
    '15' : "involved",
    '16' : "privacy",
    '17' : "disclamer",
}

l = {
    '0':"eng",
    '1':"ab",
}

class Textbox(models.Model):
    page = models.IntegerField(choices=PAGES, default=0)
    box_no = models.IntegerField(default=0)
    content_body = models.TextField()
    status =  models.IntegerField(choices=STATUS, default=0)
    lang = models.IntegerField(choices=LANG, default=0)
    
    def __str__(self):
        return 'page: ' + pgs[str(self.page)] + " - box no:" + str(self.box_no) + "- lang:" + l[str(self.lang)]
    
class Tabledata(models.Model):
    page = models.IntegerField(choices=PAGES, default=0)
    lang = models.IntegerField(choices=LANG, default=0)
    row_no = models.IntegerField(choices=LANG, default=0)
    state = models.CharField(max_length=255, blank=True)
    incidence = models.CharField(max_length=255, blank=True)
    mortality = models.CharField(max_length=255, blank=True)
    risk = models.CharField(max_length=255, blank=True)
    treatment_localized = models.CharField(max_length=255, blank=True)
    treatment_advanced = models.CharField(max_length=255, blank=True)
    treatment_metastatic = models.CharField(max_length=255, blank=True)
    
    def __str__(self):
        return 'page: ' + pgs[str(self.page)] + " - row no:" + str(self.row_no) + "- lang:" + l[str(self.lang)]
    
