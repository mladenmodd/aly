# Generated by Django 2.2.5 on 2019-09-16 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Textbox',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('page', models.IntegerField(choices=[(0, 'none'), (1, 'Publish'), (2, 'Draft'), (3, 'Publish'), (4, 'Draft'), (5, 'Publish'), (6, 'Draft'), (7, 'Publish'), (8, 'Draft'), (9, 'Publish'), (10, 'Draft'), (11, 'Publish'), (12, 'Publish'), (13, 'Publish'), (14, 'Publish')], default=0)),
                ('box_no', models.IntegerField(default=0)),
                ('content_body', models.TextField()),
                ('status', models.IntegerField(choices=[(0, 'Draft'), (1, 'Publish')], default=0)),
                ('lang', models.IntegerField(choices=[(0, 'eng'), (1, 'ab')], default=0)),
            ],
        ),
    ]
