from django.urls import path, include
from . import views

urlpatterns = [
    path('<str:lang>/search/', views.search, name='search'),
    path('<str:lang>/', views.index, name='index'),
    path('', views.index, name='index'),
    path('<str:lang>/about', views.about, name='about'),
    path('<str:lang>/prostate', views.what_is_prostate, name='Whatisprostate'),
    path('<str:lang>/prostatecancer', views.what_is_cancer, name='whatisprostatecancer'),
    path('<str:lang>/mena', views.mena, name='mena'),
    path('<str:lang>/riskfactors', views.riskfactors, name='riskfactors'),
    path('<str:lang>/screening', views.screening, name='screening'),
    path('<str:lang>/dad', views.dad, name='dad'),
    path('<str:lang>/treatment', views.treatment, name='treatment'),
    path('<str:lang>/questions', views.questions, name='questions'),
    path('<str:lang>/caring', views.caring, name='caring'),
    path('<str:lang>/journey', views.journey, name='journey'),
    path('<str:lang>/index', views.index, name='index'),
    path('<str:lang>/well', views.well, name='well'),
    path('<str:lang>/involved', views.involved, name='involved'),
    path('<str:lang>/privacy', views.privacy, name='privacy'),
    path('<str:lang>/disclamer', views.disclamer, name='disclamer'),
    
]
