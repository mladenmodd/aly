#-*- coding: utf-8 -*-
from django.shortcuts import render
from .models import Textbox, Tabledata
import re
from bs4 import BeautifulSoup as soup




def argsBuild(lang, page):
    if lang=='en':
        la = 0
    else:
        la = 1
        
    textbox = Textbox.objects.filter(lang=la, page=page)
    args = {'lang': lang }
    i=0
    for item in textbox:
        args['textbox' + str(i)] = item.content_body
        i=i+1
    return args

class CleanResult():
    pgs = {
            '0' : "none",
            '1' : "index",
            '2' : "about",
            '3' : "prostate",
            '4' : "prostatecancer",
            '5' : "mena",
            '6' : "riskfactors",
            '7' : "screening",
            '8' : "dad",
            '9' : "treatment",
            '10' : "questions",
            '11' : "caring",
            '12' : "journey",
            '13' : "index",
            '14' : "well",
            '15' : "involved",
            '16' : "privacy",
            '17' : "disclamer",
            }
    TAG_RE = re.compile(r'<[^>]+>')

    def remove_tags(self, text):
            return self.TAG_RE.sub('', text)

    def __init__(self, result, lang):
        self.url = result.page
        self.body = result.content_body
        self.lang =  lang
        self.clean_url()
        self.clean_body()
        
    def clean_url(self):
        for key in self.pgs.keys():
            if key == str(self.url):
                self.url = '/{}/{}'.format(self.lang,self.pgs[key])
                
    def clean_body(self):      
        supa  = soup(self.body, 'html.parser')
        tekst = supa.text
        self.body = tekst[:250]
        self.body = self.remove_tags(self.body[:600])
        
# Create your views here.



def index(request, lang='en'):
    args = argsBuild(lang,1)
    template_name ='blog/templates/index.html'
    return render(request, template_name, args)

def about(request, lang='en'):
    args = argsBuild(lang,2)    
    print(args)
    template_name ='blog/templates/about.html'
    return render(request, template_name, args)

def what_is_prostate(request, lang='en'):
    args = argsBuild(lang,3)

    template_name ='blog/templates/what.html'
    return render(request, template_name, args)

def what_is_cancer(request, lang='en'):
    args = argsBuild(lang,4)

    template_name ='blog/templates/whatispc.html'
    return render(request, template_name, args)

def mena(request, lang='en'):
    if lang=='en':
        la = 0
    else:
        la = 1
    args = argsBuild(lang,5)
    tabledata = Tabledata.objects.filter(lang=la, page=5)
    args['tabledata'] = tabledata
    template_name ='blog/templates/mena.html'
    return render(request, template_name, args)

def riskfactors(request, lang='en'):
    args = argsBuild(lang,6)

    template_name ='blog/templates/riskFactors.html'
    return render(request, template_name, args)

def screening(request, lang='en'):
    args = argsBuild(lang,7)

    template_name ='blog/templates/screening.html'
    return render(request, template_name, args)

def dad(request, lang='en'):
    args = argsBuild(lang,8)
    template_name ='blog/templates/dad.html'
    return render(request, template_name, args)

def treatment(request, lang='en'):
    args = argsBuild(lang,9)
    if lang=='en':
        la = 0
    else:
        la = 1
    tabledata = Tabledata.objects.filter(lang=la, page=9)
    args['tabledata'] = tabledata
    template_name ='blog/templates/treatment-options.html'
    return render(request, template_name, args)

def questions(request, lang='en'):
    args = argsBuild(lang,10)

    template_name ='blog/templates/questions.html'
    return render(request, template_name, args)

def caring(request, lang='en'):
    args = argsBuild(lang,11)

    template_name ='blog/templates/caring.html'
    return render(request, template_name, args)

def journey(request, lang='en'):
    args = argsBuild(lang,12)

    template_name ='blog/templates/journey.html'
    return render(request, template_name, args)



def well(request, lang='en'):
    args = argsBuild(lang,14)
   
    template_name ='blog/templates/wellbeing.html'
    return render(request, template_name, args)


def involved(request, lang='en'):
    args = argsBuild(lang,15)

    template_name ='blog/templates/involved.html'
    return render(request, template_name, args)


def privacy(request, lang='en'):
    args = argsBuild(lang,16)
    template_name ='blog/templates/privacy.html'
    return render(request, template_name, args)


   

def search(request, lang='en'):
    if request.method == "POST":
        args = {'lang': lang }
        args['result'] = []
        query = request.POST.get('q')
        result = Textbox.objects.filter(content_body__icontains=query)
        for r in result:
            args['result'].append(CleanResult(r, lang))
            print( args['result'])
            print('\n')
        template_name ='blog/templates/search.html'
        return render(request, template_name, args)

    
    else: 
        args = {'lang': lang }
        template_name ='blog/templates/search.html'
        return render(request, template_name, args)
    


def disclamer(request, lang='en'):
    args = argsBuild(lang,17)
    template_name ='blog/templates/disclamer.html'
    return render(request, template_name, args)



   


